require 'benchmark'
require 'narray'
require 'set'
# Document = Struct.new(:id,:a,:b,:c)
# documents_a = []
# documents_h = {}
# 1.upto(10_000) do |n|
#   d = Document.new(n)
#   documents_a << d
#   documents_h[d.id] = d
# end
# searchlist = Array.new(1000){ rand(10_000)+1 }
#
# Benchmark.bm(10) do |x|
#   x.report('array'){searchlist.each{|el| documents_a.any?{|d| d.id == el}} }
#   x.report('hash'){searchlist.each{|el| documents_h.has_key?(el)} }
# end

class GridPoint
  attr_reader :x,:y, :id
  def initialize(x, y, id = 0)
    @x, @y = x, y
    @id = id
  end
end

class Land
  attr_accessor :grid

  def initialize
    self.grid = {}

    100.times do |y|
      100.times do |x|
        id = y * 100 + x
        self.grid[id]= GridPoint.new(x,y, id)
      end
    end
  end

  def grid_point(x, y)
    grid.has_key?(x * 100 + y)
  end
end

class Land2
  attr_accessor :grid
  def initialize
    self.grid = []
    100.times do |y|
      100.times do |x|
        id = y * 100 + x
        self.grid << GridPoint.new(x,y,id)
      end
    end
  end

  def grid_point(x, y)
    grid[x * 100 + y]
    # grid.any?{|d| d.id == x * 100 + y }
  end
end

class Land3 #set
  attr_accessor :grid

  def initialize
    self.grid = Set.new
    100.times do |y|
      100.times do |x|
        id = y * 100 + x
        grid.add(GridPoint.new(x,y,id))
      end
    end
  end

  def grid_point(x, y)
    grid[x * 100 + y]
    # grid.any?{|d| d.id == x * 100 + y }
  end
end

class Land4
  attr_accessor :grid

  def initialize
    self.grid = NArray.object(10000)
    100.times do |y|
      100.times do |x|
        id = y * 100 + x
        self.grid[x * 100 + y] = id
      end
    end

    def grid_point(x, y)
      grid[x * 100 + y]
    end
  end
end

land  = Land.new
land2 = Land2.new
land3 = Land3.new
land4 = Land4.new


count = 1000000


Benchmark.bmbm(10) do |x|
  x.report('array') do
    count.times do
      land2.grid_point(rand(100), rand(100))
    end
  end
  x.report('narray') do
    count.times do
      land2.grid_point(rand(100), rand(100))
    end
  end
  x.report('hash') do
    count.times do
      land.grid_point(rand(100), rand(100))
    end
  end
end
