require 'road'
require 'building'

class City

  attr_accessor :land, :roads, :buildings

  def initialize(land, *args)
    self.roads      = []
    self.buildings  = []
    self.land       = land
  end

  def build_main_road
    y = 45

    self.roads << road = Road.new do |r|
      r.name = "Main"
      r.from = [50,45]
      r.to   = [60,45]

      (50..60).each do |x|

        r.grid << land.set_grid_point(x, y, :road)
      end
    end

    build_city_hall(55,46)

    road
  end

  def city_hall
    buildings.first
  end

  def build_city_hall(x, y)
    self.buildings << hall = Building.new do |b|
      b.name = "City Hall"

      b.grid_point = land.set_grid_point(x, y, :city_hall)
    end
  end

  def desirable_points
    points = []

    roads.map(&:grid).flatten.each do |road_point|
      land.surrounding_points(road_point).each do |p|
        if p.unoccupied?
          des = desirability(p)
          points << [des, p] if des > 0
        end
      end
    end

    points
  end

  def desirability(point)
    sum = 0
    land.surrounding_points(point).each do |p|
      if p.occupied? && building = buildings.find { |f| f.grid_point == p }
        sum += building.desirability
      end
    end

    sum
  end

  def inhabitants
    0
  end
end
