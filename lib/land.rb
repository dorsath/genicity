require 'grid_point'

class Land

  attr_accessor :cities, :grid, :width, :height

  def initialize
    self.width = self.height = 100
    self.cities = []
    self.grid = []

    set_grid!
  end

  def set_grid!
    height.times do |y|
      width.times do |x|
        grid << GridPoint.new(:x => x, :y => y, :type => :grass)
      end
    end
  end

  def start_city(*args)
    self.cities << city = City.new(self, *args)

    city
  end

  def build?(x, y)
    point = grid_point(x, y)

    (point.type != :water && point.occupation.nil?)
  end

  def grid_point(x, y)
    grid[grid_coordinates(x,y)]
  end

  def set_grid_point(x, y, object)
    point = grid_point(x, y)
    point.occupation = object

    point
  end

  def grid_coordinates(x, y)
    y * 100 + x
  end

  def point_above(point)
    grid_point(point.x, point.y - 1)
  end

  def point_below(point)
    grid_point(point.x, point.y + 1)
  end

  def point_left(point)
    grid_point(point.x - 1, point.y)
  end

  def point_right(point)
    grid_point(point.x + 1, point.y)
  end

  def surrounding_points(p)
    [point_above(p), point_below(p), point_left(p), point_right(p)]
  end

end
