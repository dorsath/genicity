class Road
  attr_accessor :name, :from, :to, :grid

  def initialize
    self.grid = []
    yield self if block_given?
  end

  def length
    ((to[0] - from[0]).abs + (to[1] - from[1]).abs) * GRID_POINT_SIZE
  end

  def desirability
    0
  end

end
