class Building

  attr_accessor :name, :grid_point

  def initialize
    yield self if block_given?
  end

  def desirability
    2
  end
end
