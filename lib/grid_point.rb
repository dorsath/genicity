class GridPoint
  attr_accessor :type, :occupation
  attr_reader :position

  def initialize hash
    @position = [hash[:x], hash[:y]]
    self.type = hash[:type]
  end

  def x
    position[0]
  end

  def y
    position[1]
  end

  def occupied?
    occupation
  end

  def unoccupied?
    occupation.nil?
  end
end
