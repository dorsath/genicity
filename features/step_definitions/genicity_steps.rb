When /^I generate land$/ do
  @land = Land.new
end

Then /^I should have an empty land$/ do
  @land.cities.should be_empty
  @land.grid.size.should == 10000
end

Given /^I have a land$/ do
  @land = Land.new
end

When /^I start a new city$/ do
  @land.start_city
end

Then /^I should have a new city on that land$/ do
  @land.cities.count.should == 1
end

Given /^I am a empty city$/ do
  @land = Land.new
  @city = @land.start_city
end

When /^years pass$/ do
end

Then /^I should be a city of (\d+) inhabitants$/ do |number|
  @city.inhabitants.should == number
end
