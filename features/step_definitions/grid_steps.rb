Given /^I have an unassigned point$/ do
  @land = Land.new
  @grid_point = @land.grid[0]
end

When /^I assign a type$/ do
  @grid_point.type = :grass
end

Then /^the point should have the forementioned type$/ do
  @grid_point.type.should == :grass
end

Given /^I have an empty point$/ do
  @land = Land.new
  @grid_point = @land.grid[50]
end

When /^the city wants to build$/ do
  @city = @land.start_city
end

Then /^I will give my permission$/ do
  @land.build?(*@grid_point.position).should == true
end

Given /^I have a tile$/ do
  @land = Land.new
  @grid_point = @land.grid[55]
end

Given /^forementioned tile has the type water$/ do
  @grid_point.type = :water
end

Then /^I won't allow the build$/ do
  @land.build?(*@grid_point.position).should == false
end

Given /^I have an occupied point$/ do
  @land = Land.new
  @grid_point = @land.grid[56]
  @grid_point.type = :grass
  @land.set_grid_point(*@grid_point.position, :house)
end

Then /^I will not give my permission$/ do
  @land.build?(*@grid_point.position).should == false
end

Then /^I should know the points around$/ do
  surrounding_points = @land.surrounding_points(@land.grid_point(20,5))
  surrounding_points.size.should == 4
  surrounding_points.map(&:position).should == [[20,4],[20,6],[19,5],[21,5]]
end
