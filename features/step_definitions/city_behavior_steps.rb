Given /^I am empty$/ do
  @land = Land.new
  @city = @land.start_city
end

When /^I build a street$/ do
  @main_road = @city.build_main_road
end

Then /^I should have a street called "([^"]*)"$/ do |road_name|
  @main_road.name.should == road_name
end

Then /^it should be (\d+)m long$/ do |arg1|
  @main_road.length.should == 100
end

Given /^I am a new city$/ do
  @land = Land.new
  @city = @land.start_city
end

Given /^I have a road$/ do
  @city.build_main_road
end

Then /^I should have a main hall$/ do
  @city.city_hall.name.should == "City Hall"
end

When /^I want to find the most desireable point$/ do
  @land = Land.new
  @city = @land.start_city
  @city.build_main_road
end

Then /^I will find it near high valued buildings$/ do
  city_hall = @city.city_hall.grid_point

  @points = [
    [2,@land.point_left(city_hall)],
    [2,@land.point_right(city_hall)]
  ]

  @city.desirable_points.should == @points
end

Then /^next to a road$/ do
  pending
  @points.each do |p|
    # @city.should == true
  end
end

Given /^I have jobs or a population under (\d+)$/ do |arg1|
  pending # express the regexp above with the code you wish you had
end

Then /^I should have a need for housing$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^I have infrastructure$/ do
  pending # express the regexp above with the code you wish you had
end

Given /^space to build next to a road$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should assign a plot of land for buildings$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I have a need for housing$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should assign land for residence on the most desireable locale$/ do
  pending # express the regexp above with the code you wish you had
end

When /^I have a need for jobs$/ do
  pending # express the regexp above with the code you wish you had
end

Then /^I should assign land for jobs on the most desirable locale$/ do
  pending # express the regexp above with the code you wish you had
end
