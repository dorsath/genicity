Feature: Genicity
  In order to watch my city grow
  As a player
  I want to generate new land and start a city

  Scenario: Generate land
    When I generate land
    Then I should have an empty land

  Scenario: Start city
    Given I have a land
    When I start a new city
    Then I should have a new city on that land

  Scenario: Grow to ginormous city
    Given I am a empty city
    When years pass
    Then I should be a city of 20000 inhabitants


