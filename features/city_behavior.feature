Feature: City behavior
  In order to expand
  As a city
  I want to designate building land and roads

  Scenario: Create mainstreet
    Given I am empty
    When I build a street
    Then I should have a street called "Main"
    And it should be 100m long

  Scenario: Create City Hall
    Given I am a new city
    And I have a road
    Then I should have a main hall

  Scenario: Determine desireablity
    When I want to find the most desireable point
    Then I will find it near high valued buildings
    And next to a road

  Scenario: Determine need for housing
    Given I have jobs or a population under 50
    Then I should have a need for housing

  Scenario: Allow the build of buildings
    Given I have infrastructure
    And space to build next to a road
    Then I should assign a plot of land for buildings

  Scenario: Meet housing needs
    When I have a need for housing
    Then I should assign land for residence on the most desireable locale

  Scenario: Meet job needs
    When I have a need for jobs
    Then I should assign land for jobs on the most desirable locale
