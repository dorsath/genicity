Feature: Grid, streams of information
  In order to allow construction
  As a grid
  I should have grid points to assign to purpose

  Scenario: Assign type to gridpoint
    Given I have an unassigned point
    When I assign a type
    Then the point should have the forementioned type

  Scenario: Building
    Given I have an empty point
    When the city wants to build
    Then I will give my permission

  Scenario: Type Water
    Given I have a tile
    And forementioned tile has the type water
    When the city wants to build
    Then I won't allow the build

  Scenario: Determine possibility of construction on occupied land
    Given I have an occupied point
    When the city wants to build
    Then I will not give my permission

  Scenario: Find grid points around a grid point
    Given I have an occupied point
    Then I should know the points around
